<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $rooms app\models\Room[] */

$this->title = 'Rooms';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="room-index">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->user->can('admin')): ?>
        <p>
            <?= Html::a('Create Room', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php endif; ?>

    <?php foreach ($rooms as $room): ?>
        <div class="card" style="width: 18rem; margin: 50px 50px 50px 0px; float: left;">
            <div class="card-body">
                <h5 class="card-title"><?= Html::encode($room->name) ?></h5>
                <p class="card-title"><?= Html::encode($room->description) ?></p>
                <?php if (Yii::$app->user->can('admin')): ?>
                    <?= Html::a('View', ['view', 'id' => $room->id], ['class' => 'btn btn-primary btn-sm']) ?>
                    <?= Html::a('Update', ['update', 'id' => $room->id], ['class' => 'btn btn-warning btn-sm']) ?>
                    <?= Html::a('Delete', ['delete', 'id' => $room->id], ['class' => 'btn btn-danger btn-sm']) ?>
                    <?= Html::a('List Bookings', ['booking/list', 'room_id' => $room->id], ['class' => 'btn btn-info btn-sm']) ?>
                <?php endif; ?>

                <?php
                $userHasBooked = \app\models\Booking::find()
                    ->where(['room_id' => $room->id, 'user_id' => Yii::$app->user->id])
                    ->exists();

                if (Yii::$app->user->can('user')):
                    if (!$userHasBooked) {
                            echo Html::a('Book', ['booking/create', 'room_id' => $room->id], ['class' => 'btn btn-success btn-sm']);
                    } else {
                        echo Html::a('You have Booked this room', 'javascript:void(0);', ['class' => 'btn btn-success btn-sm disabled']);
                    }
                endif;
                ?>
            </div>
        </div>
    <?php endforeach; ?>


</div>
