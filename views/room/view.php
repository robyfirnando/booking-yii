<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $room app\models\Room */

$this->title = $room->name;
$this->params['breadcrumbs'][] = ['label' => 'Rooms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="room-view">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $room->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $room->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this room?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Room: <?= Html::encode($room->name) ?></h5>
            <p class="card-text">Description: <?= Html::encode($room->description) ?></p>
            <p class="card-text">Capacity: <?= Html::encode($room->capacity) ?></p>
        </div>
    </div>
</div>
