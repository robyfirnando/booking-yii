<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Booking */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Book Room';
$this->params['breadcrumbs'][] = ['label' => 'Rooms', 'url' => ['room/index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="booking-create">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'start_time')->input('datetime-local') ?>
    <?= $form->field($model, 'end_time')->input('datetime-local') ?>

    <!-- Display error messages for each field -->
    <?php if ($model->hasErrors()): ?>
        <div class="alert alert-danger">
            <?= Html::errorSummary($model); ?>
        </div>
    <?php endif; ?>

    <div class="form-group">
        <?= Html::submitButton('Book', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>


</div>
