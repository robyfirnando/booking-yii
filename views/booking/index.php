<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use app\models\Booking;

/* @var $this yii\web\View */

$this->title = 'Bookings';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="booking-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= GridView::widget([
        'dataProvider' => new ActiveDataProvider([
            'query' => Booking::find(), // This should be a Query object
            'pagination' => [
                'pageSize' => 20, // Adjust the pageSize as needed
            ],
        ]),
        'columns' => [
            'id',
            'user_id',
            'room_id',
            'start_time',
            'end_time',
            // Other necessary columns
            ['class' => 'yii\grid\ActionColumn'], // Optional, for view, update, delete buttons
        ],
    ]); ?>
</div>
