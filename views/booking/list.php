<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $bookings app\models\Booking[] */
/* @var $room_id int */

$this->title = 'List of Bookings for Room ' . $room_id;
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="booking-list">
    <h1><?= Html::encode($this->title) ?></h1>

    <table class="table table-bordered">
    <thead>
        <tr>
            <th>User Name</th>
            <th>Start Time</th>
            <th>End Time</th>
            <th>Status</th>
            <th>Action</th> <!-- New column for action button -->
        </tr>
    </thead>
    <tbody>
        <?php foreach ($bookings as $booking): ?>
            <tr>
                <td><?= Html::encode($booking->user->name) ?></td>
                <td><?= Html::encode($booking->start_time) ?></td>
                <td><?= Html::encode($booking->end_time) ?></td>
                <td><?= Html::encode($booking->status) ?></td>
                <td>
                    <?php if ($booking->status === 'pending'): ?>
                        <?= Html::a('Approve', ['booking/approve', 'id' => $booking->id], [
                            'class' => 'btn btn-success btn-sm',
                            'data' => [
                                'confirm' => 'Are you sure you want to approve this booking?',
                                'method' => 'post',
                            ],
                        ]) ?>
                    <?php elseif ($booking->status === 'approved'): ?>
                        <?= Html::a('Pending', ['booking/pending', 'id' => $booking->id], [
                            'class' => 'btn btn-warning btn-sm',
                            'data' => [
                                'confirm' => 'Are you sure you want to set this booking as pending?',
                                'method' => 'post',
                            ],
                        ]) ?>
                    <?php endif; ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>
</div>