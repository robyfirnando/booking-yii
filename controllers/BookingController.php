<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\models\Booking;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\web\NotFoundHttpException;

class BookingController extends Controller
{
    public function actionIndex()
    {
        $bookings = Booking::find()->all();
        return $this->render('index', ['bookings' => $bookings]);
    }

    public function actionCreate($room_id = null)
    {
        $model = new Booking();

        if ($room_id) {
            $model->room_id = $room_id;
            $model->user_id = Yii::$app->user->id;
            $model->status = 'pending';
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                if ($model->save()) {
                    return $this->redirect(['room/index']);
                }
            } else {
                Yii::error("Validation errors: " . json_encode($model->getErrors()));
            }
        }
        

        return $this->render('create', ['model' => $model]);
    }

    public function actionView($id)
    {
        $booking = $this->findModel($id);
        return $this->render('view', ['booking' => $booking]);
    }

    public function actionList($room_id)
    {
        $bookings = Booking::find()->where(['room_id' => $room_id])->all();

        return $this->render('list', ['bookings' => $bookings, 'room_id' => $room_id]);
    }

    public function actionApprove($id)
    {
        $booking = Booking::findOne($id);
        $rules = $booking->rules();
        if ($booking) {
            $booking->status = 'approved';
            $booking->save();
        }

        return $this->redirect(['list', 'room_id' => $booking->room_id]);
    }

    public function actionPending($id)
    {
        $booking = Booking::findOne($id);
        if ($booking) {
            $booking->status = 'pending';
            $booking->save();
        }

        return $this->redirect(['list', 'room_id' => $booking->room_id]);
    }

    // Helper method to find a Booking model
    protected function findModel($id)
    {
        if (($model = Booking::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested booking does not exist.');
    }
}