<?php

namespace app\controllers;

use Yii;
use app\models\Room;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\web\NotFoundHttpException;

class RoomController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['admin']
                    ],
                    [
                        'actions' => ['index', 'view'],
                        'allow' => true,
                        'roles' => ['@'], // Logged in users
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $rooms = Room::find()->all();
        return $this->render('index', ['rooms' => $rooms]);
    }

    public function actionView($id)
    {
        $room = $this->findModel($id);
        return $this->render('view', ['room' => $room]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionCreate()
    {
        $model = new Room();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $room = $this->findModel($id);

        if ($room->getBookings()->exists()) {
            Yii::$app->session->setFlash('error', 'Cannot delete the room because there are bookings associated with it.');
            return $this->redirect(['index']);
        }

        $room->delete();
        return $this->redirect(['index']);
    }



    // Helper method to find a Room model
    protected function findModel($id)
    {
        if (($model = Room::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested room does not exist.');
    }
}
