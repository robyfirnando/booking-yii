<?php
namespace app\commands;

use yii\console\Controller;
use Yii;
use app\models\User; 

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll(); 

        $adminRole = $auth->createRole('admin');
        $auth->add($adminRole);

        $userRole = $auth->createRole('user');
        $auth->add($userRole);

        $adminUsers = User::find()->where(['level' => 'admin'])->all();
        foreach ($adminUsers as $user) {
            $auth->assign($adminRole, $user->id);
        }

        $userUsers = User::find()->where(['level' => 'user'])->all();
        foreach ($userUsers as $user) {
            $auth->assign($userRole, $user->id);
        }
    }
}
