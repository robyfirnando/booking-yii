<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class Booking extends ActiveRecord
{
    public static function tableName()
    {
        return 'bookings'; // Name of the database table
    }

    public function rules()
    {
        $rules = [
            [['room_id', 'user_id', 'start_time', 'end_time'], 'required'],
            [['room_id', 'user_id'], 'integer'],
            [['room_id'], 'validateCapacity'],
            [['start_time', 'end_time'], 'datetime', 'format' => 'php:Y-m-d\TH:i'],
            ['end_time', 'compare', 'compareAttribute' => 'start_time', 'operator' => '>', 'message' => 'End time must be after start time.'],
            // [['status'], 'string'],
            [['start_time', 'end_time'], 'validateBookingTime'],
        ];

        // if user change status booking to approve / pending then change the rules:
        if (in_array(Yii::$app->getRequest()->get('r'), ['booking/approve', 'booking/pending'])) {
            $rules = [
                [['room_id'], 'validateCapacity']
            ];
        }
        // echo '<pre>';
        // var_dump($rules);
        // die;
        return $rules;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            // Convert start_time and end_time to your database format
            $this->start_time = date('Y-m-d H:i:s', strtotime($this->start_time));
            $this->end_time = date('Y-m-d H:i:s', strtotime($this->end_time));

            return true;
        }
        return false;
    }

    // public function validateCapacity($attribute, $params)
    // {
    //     $room = Room::findOne($this->room_id);
    //     if ($room && $this->capacity > $room->capacity) {
    //         $this->addError($attribute, 'Capacity exceeds the room\'s capacity.');
    //     }
    // }

    // check wether the capacity is full or not
    public function validateCapacity($attribute, $params)
    {
        $room = Room::findOne($this->room_id);

        if (!$room) {
            $this->addError($attribute, 'Invalid room selected.');
            return;
        }

        // Count the total number of people in bookings for the room
        $totalPeopleInBookings = Booking::find()
            ->where(['room_id' => $this->room_id])
            ->count();

        $capacity = $room->capacity;
        if (empty($capacity)) {
            $capacity = 1;
        }
        $availableCapacity = $capacity - $totalPeopleInBookings;

        if ($totalPeopleInBookings > $availableCapacity) {
            $this->addError($attribute, 'Booking capacity exceeds room capacity.');
        }
    }

    public function validateBookingTime($attribute, $params)
    {
        if (!$this->isNewRecord) {
            return;
        }

        $overlapCount = Booking::find()
            ->where(['room_id' => $this->room_id])
            // ->andWhere(['!=', 'id', $this->id]) // This line can be removed as isNewRecord is true
            ->andWhere(['<', 'start_time', $this->end_time])
            ->andWhere(['>', 'end_time', $this->start_time])
            ->count();

        if ($overlapCount > 0) {
            $this->addError($attribute, 'This time slot is already booked.');
        }
    }



    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'room_id' => 'Room ID',
            'user_id' => 'User ID',
            'start_time' => 'Start Time',
            'end_time' => 'End Time',
        ];
    }

    // Custom validation method to check for overlapping bookings
    // public function validateBooking($attribute, $params)
    // {
    //     $overlapCount = Booking::find()
    //         ->where(['room_id' => $this->room_id])
    //         ->andWhere(['!=', 'id', $this->id]) // Exclude current booking from check
    //         ->andWhere(['<', 'start_time', $this->end_time])
    //         ->andWhere(['>', 'end_time', $this->start_time])
    //         ->count();

    //     if ($overlapCount > 0) {
    //         $this->addError($attribute, 'This time slot is already booked.');
    //     }
    // }

    // Relation to Room model (if needed)
    public function getRoom()
    {
        return $this->hasOne(Room::className(), ['id' => 'room_id']);
    }

    // Relation to User model (if needed)
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
