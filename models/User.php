<?php
namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class User extends ActiveRecord implements IdentityInterface
{
    public function rules()
    {
        return [
            [['level'], 'string'], 
        ];
    }

    public function getAuthKey()
    {
        return 'dummyAuthKey';
    }

    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    // public function getAuthKey()
    // {
    //     // Implement this method if you're using authKey
    //     // Otherwise, you can return a null or a dummy value
    //     return $this->authKey;
    // }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        // Implement this method if you're using authKey
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password); // Assuming 'password_hash' is the field in your database
    }
}
