<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class Room extends ActiveRecord
{
    public static function tableName()
    {
        return 'rooms'; // Name of the database table
    }

    public function rules()
    {
        return [
            [['name', 'description', 'capacity'], 'required'], // Both fields are required
            [['name'], 'string', 'max' => 255], // Name is a string with a maximum length of 255
            [['description'], 'string'], // Description is a text field
            [['capacity'], 'integer', 'min' => 1],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'capacity' => 'Capacity'
        ];
    }

    public function getBookings()
    {
        return $this->hasMany(Booking::className(), ['room_id' => 'id']);
    }

}
