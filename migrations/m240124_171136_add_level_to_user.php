<?php

use yii\db\Migration;

/**
 * Class m240124_171136_add_level_to_user
 */
class m240124_171136_add_level_to_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('users', 'level', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m240124_171136_add_level_to_user cannot be reverted.\n";
        $this->dropColumn('users', 'level');
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m240124_171136_add_level_to_user cannot be reverted.\n";

        return false;
    }
    */
}
